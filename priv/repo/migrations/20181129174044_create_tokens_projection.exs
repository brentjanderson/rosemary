defmodule Rosemary.Repo.Migrations.CreateTokensProjection do
  @moduledoc """
  Create projection to track user tokens
  """
  use Ecto.Migration

  def change do
    create table(:users_tokens, primary_key: false) do
      add :user_id, :uuid, primary_key: true
      add :token, :string, primary_key: true
      add :expires, :utc_datetime

      timestamps()
    end

    create unique_index(:users_tokens, [:user_id, :token])
  end
end
