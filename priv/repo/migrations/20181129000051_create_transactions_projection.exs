defmodule Rosemary.Repo.Migrations.CreateTransactionsProjection do
  @moduledoc """
  Create projection for transactions collection
  """

  use Ecto.Migration

  def change do
    create table(:transactions, primary_key: false) do
      add :txn_id, :uuid, primary_key: true
      add :user_id, :uuid
      add :merchant, :string
      add :category, :string
      add :amount, :integer
      add :date, :date

      timestamps()
    end
  end
end
