use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :rosemary, RosemaryWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :rosemary, Rosemary.Repo,
  username: "postgres",
  password: "password",
  database: "rosemary_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :commanded, event_store_adapter: Commanded.EventStore.Adapters.Extreme

config :commanded_extreme_adapter,
  serializer: Commanded.Serialization.JsonSerializer,
  stream_prefix: "commandedtest"

config :extreme, :event_store,
  db_type: :node,
  host: "localhost",
  port: 1113,
  username: "admin",
  password: "changeit",
  reconnect_delay: 2_000,
  max_attempts: :infinity
