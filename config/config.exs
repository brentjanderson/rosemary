# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :rosemary,
  ecto_repos: [Rosemary.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :rosemary, RosemaryWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "E1IGbS6Mtl61QJKT2gyYG8XvI281G2K8MUEWIo9fRO6bR7RVTA/GionVTFEZ8/Wl",
  render_errors: [view: RosemaryWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Rosemary.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :sentry,
  dsn: "https://620e686d28c142debe840403fa33d4ad@sentry.io/1321626",
  included_environments: [:prod, :dev],
  environment_name: Mix.env()

config :email_checker,
  default_dns: :system,
  also_dns: [],
  validations: [EmailChecker.Check.Format, EmailChecker.Check.MX],
  smtp_retries: 2,
  timeout_milliseconds: :infinity

config :commanded_ecto_projections,
  repo: Rosemary.Repo

config :vex,
  sources: [
    [date: Rosemary.Support.Validators.Date, datetime: Rosemary.Support.Validators.DateTime],
    Rosemary.Users.Validators,
    Rosemary.Support.Validators,
    Vex.Validators
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
