import React from "react";
import ReactDOM from "react-dom";

import { InMemoryCache } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";
import { ApolloLink } from "apollo-link";
import { ApolloClient } from "apollo-client";
import { withClientState } from "apollo-link-state";
import { ApolloProvider } from "react-apollo";

import css from "../css/app.css";
import { App } from "./components";

const cache = new InMemoryCache();

const httpLink = new HttpLink({ uri: "/api/graphql" });

const authMiddleware = new ApolloLink((operation, forward) => {
  const token = localStorage.getItem("_authToken") || null;
  operation.setContext({ headers: { authorization: `Bearer ${token}` } });

  return forward(operation);
});

const stateLink = withClientState({
  cache,
  defaults: { authenticationToken: "" }
});

const client = new ApolloClient({
  link: ApolloLink.from([stateLink, authMiddleware, httpLink]),
  cache,
  clientState: {
    defaults: {
      authenticationToken: localStorage.getItem("_authToken") || ""
    }
  }
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById("root")
);
