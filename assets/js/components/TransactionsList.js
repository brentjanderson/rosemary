import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import romanize from "romanize";

const TXN_LIST = gql`
  {
    transactions {
      id
      date
      category
      merchant
      amount
    }
  }
`;

const TransactionsList = ({ romanized }) => (
  <Query query={TXN_LIST} pollInterval={1000}>
    {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Error :(</p>;

      if (data.transactions.length === 0) {
        return <h3>Add some transactions to get started!</h3>;
      }

      const rows = data.transactions.map(txn => {
        const amount = txn.amount / 100;
        const formattedAmount = romanized ? romanize(amount) : amount;
        return (
          <tr key={txn.id}>
            <td>{txn.date}</td>
            <td>{txn.merchant}</td>
            <td>{txn.category}</td>
            <td>{`$${formattedAmount}`}</td>
          </tr>
        );
      });

      return (
        <table>
          <thead>
            <tr>
              <th>Date</th>
              <th>Merchant</th>
              <th>Category</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
      );
    }}
  </Query>
);

export { TransactionsList };
