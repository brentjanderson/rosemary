import React from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

const REGISTER_USER = gql`
  mutation registerUser($email: String!, $password: String!) {
    registerUser(email: $email, password: $password) {
      id
      email
    }
  }
`;

const RegisterForm = () => {
  let emailInput;
  let passwordInput;

  return (
    <Mutation mutation={REGISTER_USER}>
      {(loginUser, { data, client }) => (
        <div>
          <h1>Register</h1>
          <form
            onSubmit={e => {
              e.preventDefault();
              loginUser({
                variables: {
                  email: emailInput.value,
                  password: passwordInput.value
                }
              })
                .then(({ data: { registerUser: { email } } }) => {
                  alert(`Account ${email} registered. You may now login.`);
                })
                .catch(e => {
                  alert(e.message);
                  console.error(e);
                });
            }}
          >
            <fieldset>
              <label htmlFor="emailField">Email</label>
              <input
                ref={node => {
                  emailInput = node;
                }}
                type="email"
                placeholder="you@example.com"
                id="emailField"
                autoFocus
              />
              <label htmlFor="passwordField">Password</label>
              <input
                ref={node => {
                  passwordInput = node;
                }}
                type="password"
                id="passwordField"
              />
              <input
                className="button-primary"
                type="submit"
                value="Register"
              />
            </fieldset>
          </form>
        </div>
      )}
    </Mutation>
  );
};

export { RegisterForm };
