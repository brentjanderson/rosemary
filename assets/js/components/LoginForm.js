import React from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

const LOGIN_USER = gql`
  mutation loginUser($email: String!, $password: String!) {
    loginUser(email: $email, password: $password) {
      id
      token
    }
  }
`;

const LoginForm = () => {
  let emailInput;
  let passwordInput;

  return (
    <Mutation mutation={LOGIN_USER}>
      {(loginUser, { data, client }) => (
        <div>
          <h1>Log In</h1>
          <form
            onSubmit={e => {
              e.preventDefault();
              loginUser({
                variables: {
                  email: emailInput.value,
                  password: passwordInput.value
                }
              })
                .then(({ data: { loginUser: { token } } }) => {
                  client.writeData({ data: { authenticationToken: token } });
                  localStorage.setItem("_authToken", token);
                })
                .catch(e => {
                  console.error(e);
                });
            }}
          >
            <fieldset>
              <label htmlFor="emailField">Email</label>
              <input
                ref={node => {
                  emailInput = node;
                }}
                type="email"
                placeholder="you@example.com"
                id="emailField"
                autoFocus
              />
              <label htmlFor="passwordField">Password</label>
              <input
                ref={node => {
                  passwordInput = node;
                }}
                type="password"
                id="passwordField"
              />
              <input className="button-primary" type="submit" value="Log In" />
            </fieldset>
          </form>
        </div>
      )}
    </Mutation>
  );
};

export { LoginForm };
