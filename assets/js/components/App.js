import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

import { TransactionsPage, LoginForm, RegisterForm } from "./index";

const App = () => (
  <div>
    <h1>Rosemary Budgeting App</h1>
    <blockquote>Because it's not mint</blockquote>
    <Query
      query={gql`
        {
          authenticationToken @client
        }
      `}
    >
      {({ loading, error, data }) => {
        if (loading) return <p>Loading...</p>;
        if (error) return <p>Error :(</p>;

        if (
          data !== undefined &&
          data.authenticationToken !== undefined &&
          data.authenticationToken !== ""
        ) {
          return <TransactionsPage />;
        } else {
          return (
            <div>
              <LoginForm />
              <RegisterForm />
            </div>
          );
        }
      }}
    </Query>
  </div>
);

export { App };
