import React from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

const ADD_TXN = gql`
  mutation AddTxn(
    $date: String!
    $merchant: String!
    $category: String!
    $amount: String!
  ) {
    addTxn(
      date: $date
      merchant: $merchant
      category: $category
      amount: $amount
    ) {
      id
      date
      merchant
      category
    }
  }
`;

const TransactionForm = () => {
  let dateField;
  let merchantField;
  let categoryField;
  let amountField;

  return (
    <Mutation mutation={ADD_TXN}>
      {(addTxn, { data }) => (
        <form>
          <fieldset>
            <label htmlFor="dateField">Date</label>
            <input
              ref={node => {
                dateField = node;
              }}
              type="date"
              name="dateField"
              id="dateField"
            />
            <label htmlFor="merchantField">Merchant</label>
            <input
              ref={node => {
                merchantField = node;
              }}
              type="text"
              name="merchantField"
              id="merchantField"
            />
            <label htmlFor="categoryField">Category</label>
            <input
              ref={node => {
                categoryField = node;
              }}
              type="text"
              name="categoryField"
              id="categoryField"
            />
            <label htmlFor="amountField">Amount</label>
            <input
              ref={node => {
                amountField = node;
              }}
              type="number"
              min="0"
              name="amountField"
              id="amountField"
            />
            <button
              onClick={e => {
                e.preventDefault();
                addTxn({
                  variables: {
                    date: dateField.value,
                    merchant: merchantField.value,
                    category: categoryField.value,
                    amount: amountField.value * 100 // Store in cents
                  }
                }).then(() => {
                  dateField.value = "";
                  merchantField.value = "";
                  categoryField.value = "";
                  amountField.value = "";
                });
              }}
              type="button"
              className="button"
            >
              Add Transaction
            </button>
          </fieldset>
        </form>
      )}
    </Mutation>
  );
};

export { TransactionForm };
