import React from "react";

import { TransactionsList, TransactionForm } from "./index";

class TransactionsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { romanized: false };
  }

  render() {
    return (
      <div>
        <h1>Transactions</h1>
        <label htmlFor="romanized">Roman numerals</label>
        <input
          defaultChecked={this.state.romanized}
          type="checkbox"
          id="romanized"
          onChange={e => {
            console.log(e.target.value);
            console.log(e.target.checked);
            this.setState({ romanized: !this.state.romanized });
          }}
        />

        <TransactionsList romanized={this.state.romanized} />
        <TransactionForm />
      </div>
    );
  }
}

export { TransactionsPage };
