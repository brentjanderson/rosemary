defmodule Rosemary.Router do
  @moduledoc """
  Router to map event sourcing commands to aggregates.

  For more information, see [Commanded's Documentation](https://github.com/commanded/commanded/blob/master/guides/Commands.md#command-dispatch-and-routing)
  """

  use Commanded.Commands.Router

  alias Rosemary.Transactions.Aggregates.Transaction
  alias Rosemary.Transactions.Commands.{AddTransaction}
  alias Rosemary.Users.Aggregates.User
  alias Rosemary.Users.Commands.{ExpireToken, GenerateToken, RegisterUser}
  alias Rosemary.Support.Middleware.{Validate}

  middleware(Validate)

  identify(User, by: :user_id, prefix: "user-")
  identify(Transaction, by: :txn_id, prefix: "txn-")

  dispatch(RegisterUser, to: User)
  dispatch(GenerateToken, to: User)
  dispatch(ExpireToken, to: User)

  dispatch(AddTransaction, to: Transaction)
end
