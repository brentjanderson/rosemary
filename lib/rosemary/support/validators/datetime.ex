defmodule Rosemary.Support.Validators.DateTime do
  @moduledoc """
  Vex Validator for date type
  """

  use Vex.Validator
  alias Vex.Validators.By

  def validate(value, _options) do
    By.validate(value, function: &is_a_datetime?/1)
  end

  defp is_a_datetime?(%DateTime{}) do
    true
  end

  defp is_a_datetime?(_) do
    false
  end
end
