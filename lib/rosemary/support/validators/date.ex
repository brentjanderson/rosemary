defmodule Rosemary.Support.Validators.Date do
  @moduledoc """
  Vex Validator for date type
  """

  use Vex.Validator
  alias Vex.Validators.By

  def validate(value, _options) do
    By.validate(value, function: &is_a_date?/1)
  end

  defp is_a_date?(%Date{}) do
    true
  end

  defp is_a_date?(_) do
    false
  end
end
