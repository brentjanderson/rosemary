defmodule Rosemary.Repo do
  use Ecto.Repo,
    otp_app: :rosemary,
    adapter: Ecto.Adapters.Postgres
end
