defmodule Rosemary.Schema do
  @moduledoc """
  GraphQL Schema definitions
  """

  use Absinthe.Schema

  alias Rosemary.Resolvers

  import_types(Rosemary.Schema.ItemTypes)

  query do
    field :transactions, list_of(:transaction) do
      resolve(&Resolvers.list_transactions/3)
    end
  end

  mutation do
    field :register_user, type: :user do
      arg(:email, non_null(:string))
      arg(:password, non_null(:string))
      resolve(&Resolvers.register_user/3)
    end

    field :login_user, type: :user_token do
      arg(:email, non_null(:string))
      arg(:password, non_null(:string))
      resolve(&Resolvers.login_user/3)
    end

    field :add_txn, type: :transaction do
      arg(:date, non_null(:string))
      arg(:merchant, non_null(:string))
      arg(:category, non_null(:string))
      arg(:amount, non_null(:integer))

      resolve(&Resolvers.add_txn/3)
    end
  end
end
