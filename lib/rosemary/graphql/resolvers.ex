defmodule Rosemary.Resolvers do
  @moduledoc """
  Resolvers for GraphQL Query resolution
  """

  alias Rosemary.{Transactions, Users}

  def register_user(_parent, %{email: email, password: password}, _info) do
    case Users.register_user(email, password) do
      {:ok, result} -> {:ok, format_user(result)}
      {:error, :validation_failure, fields} -> {:error, format_validation_errors(fields)}
      {:error, reason} -> {:error, reason}
    end
  end

  def register_user(_parent, _args, _info) do
    {:error, :invalid_request}
  end

  def login_user(_parent, %{email: email, password: password}, _info) do
    with {:ok, token} <- Users.login_user(email, password),
         %{token: token, expires: expires, user_id: _user_id} <- Map.from_struct(token) do
      {:ok, %{token: token, expires: expires, id: UUID.uuid4()}}
    else
      {:error, fields} -> {:error, format_validation_errors(fields)}
      _ -> {:error, :login_error}
    end
  end

  def login_user(_parent, _args, _info) do
    {:error, :invalid_request}
  end

  def add_txn(
        _parent,
        %{amount: amount, category: category, date: date, merchant: merchant},
        %{context: %{user_id: user_id}}
      ) do
    with {:ok, parsed_date} <- Date.from_iso8601(date),
         {:ok, txn} <-
           Transactions.add_transaction(user_id, category, merchant, amount, parsed_date),
         map_txn <- format_txn(txn) do
      {:ok, map_txn}
    else
      _ -> {:error, :unable_to_add_txn}
    end
  end

  def add_txn(_parent, _args, _info) do
    {:error, :not_authorized}
  end

  def list_transactions(_parent, _args, %{context: %{user_id: user_id}}) do
    {:ok, txns} = Transactions.get_user_transactions(user_id)

    formatted_txns =
      txns
      |> Enum.map(fn %{
                       amount: amount,
                       category: category,
                       date: date,
                       merchant: merchant,
                       txn_id: txn_id,
                       user_id: user_id
                     } ->
        %{
          id: txn_id,
          amount: amount,
          category: category,
          date: date,
          merchant: merchant,
          user_id: user_id
        }
      end)

    {:ok, formatted_txns}
  end

  def list_transactions(_parent, _args, _info) do
    {:error, :not_authorized}
  end

  defp format_user(%{email: email, user_id: user_id}) do
    %{id: user_id, email: email}
  end

  defp format_txn(%{
         txn_id: txn_id,
         user_id: user_id,
         category: category,
         merchant: merchant,
         amount: amount,
         date: date
       }) do
    %{
      id: txn_id,
      user_id: user_id,
      category: category,
      merchant: merchant,
      amount: amount,
      date: date
    }
  end

  defp format_validation_errors(fields) when is_atom(fields) do
    fields
  end

  defp format_validation_errors(fields) do
    fields
    |> Enum.map(fn {key, reasons} ->
      %{message: "The '#{Atom.to_string(key)}' field was invalid", reasons: reasons}
    end)
  end
end
