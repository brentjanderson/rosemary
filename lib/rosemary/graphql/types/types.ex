defmodule Rosemary.Schema.ItemTypes do
  @moduledoc """
  Type definitions for our GraphQL Schema
  """

  use Absinthe.Schema.Notation

  @desc "A user's transaction"
  object :transaction do
    field :id, :id
    field :date, :string
    field :category, :string
    field :merchant, :string
    field :user, :user
    field :amount, :integer
  end

  @desc "A user"
  object :user do
    field :id, :id
    field :email, :string
  end

  object :user_token do
    field :id, :id
    field :token, :string
    field :user, :user
  end
end
