defmodule Rosemary.Users.Events.TokenGenerated do
  @moduledoc """
  Event to reflect user registered
  """

  defstruct [:user_id, :token, :expires]
end
