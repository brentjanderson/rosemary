defmodule Rosemary.Users.Events.UserRegistered do
  @moduledoc """
  Event to reflect user registered
  """

  defstruct [:user_id, :email, :hashed_password]
end
