defmodule Rosemary.Users.Events.TokenExpired do
  @moduledoc """
  Event to reflect a user registration token that has expired
  """

  defstruct [:user_id, :token]
end
