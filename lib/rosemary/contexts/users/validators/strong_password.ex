defmodule Rosemary.Users.Validators.StrongPassword do
  @moduledoc """
  Validator for email uniqueness
  """

  use Vex.Validator

  def validate(value, _context) do
    case Rosemary.Auth.check_password_strength(value) do
      :ok -> :ok
      {:error, error} -> {:error, error}
    end
  end
end
