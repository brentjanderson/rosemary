defmodule Rosemary.Users.Validators.ValidEmail do
  @moduledoc """
  Validator for email service provider
  """

  use Vex.Validator

  alias Rosemary.Users
  alias Rosemary.Users.Projections.User

  def validate(value, _context) do
    with :ok <- check_email_valid(value),
         :ok <- check_email_burner(value),
         :ok <- check_unique_email(value) do
      :ok
    else
      {:error, err} -> {:error, err}
      _ -> {:error, :error_validating_email}
    end
  end

  defp check_email_valid(email) do
    case EmailChecker.valid?(email) do
      true -> :ok
      false -> {:error, :email_address_not_valid}
    end
  end

  defp check_email_burner(email) do
    case Burnex.is_burner?(email) do
      false -> :ok
      true -> {:error, :email_address_burner}
    end
  end

  defp check_unique_email(email) do
    case Users.user_by_email(email) do
      {:error, :not_found} -> :ok
      {:ok, %User{}} -> {:error, :email_already_exists}
    end
  end
end
