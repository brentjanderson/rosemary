defmodule Rosemary.Users.Validators.ValidToken do
  @moduledoc """
  Validator for user authentication tokens
  """

  use Vex.Validator

  alias Rosemary.Users
  alias Rosemary.Users.Projections.Token

  def validate(value, _context) do
    with :ok <- check_unique_token(value) do
      :ok
    else
      {:error, err} -> {:error, err}
      _ -> {:error, :error_validating_token}
    end
  end

  defp check_unique_token(token) do
    case Users.user_for_token(token) do
      {:error, :not_found} -> :ok
      {:ok, %Token{}} -> {:error, :token_already_exists}
    end
  end
end
