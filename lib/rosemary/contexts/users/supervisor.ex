defmodule Rosemary.Users.Supervisor do
  @moduledoc """
  Supervision tree for Users context, including projections
  """

  use Supervisor

  alias Rosemary.Users.Projectors.User

  def start_link do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_arg) do
    Supervisor.init(
      [
        User
      ],
      strategy: :one_for_one
    )
  end
end
