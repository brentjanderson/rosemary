defmodule Rosemary.Users do
  @moduledoc """
  User context, to support all user functions and activity
  """

  alias Rosemary.Auth
  alias Rosemary.Repo
  alias Rosemary.Router
  alias Rosemary.Users.Commands.{ExpireToken, GenerateToken, RegisterUser}
  alias Rosemary.Users.Projections.{Token, User}
  alias Rosemary.Users.Queries.UserByEmail

  @doc """
  Register a new user in the system. Ensure duplicates are not permitted, and enforce password policies.
  """
  def register_user(email, password) do
    uuid = UUID.uuid4()

    register_user =
      %{email: email, password: password}
      |> RegisterUser.new()
      |> RegisterUser.assign_id(uuid)
      |> RegisterUser.downcase_email()
      |> RegisterUser.hash_password()

    with :ok <- Router.dispatch(register_user, consistency: :strong) do
      user_by_id(uuid)
    else
      reply -> reply
    end
  end

  @doc """
  Validates a user's email and password
  """
  def login_user(email, password) do
    email
    |> user_by_email()
    |> check_user_login(password)
    |> generate_login_token()
  end

  @doc """
  Retrieves a user by email address
  """
  def user_by_email(email) do
    user = email |> String.downcase() |> UserByEmail.new() |> Repo.one()

    case user do
      nil -> {:error, :not_found}
      user -> {:ok, user}
    end
  end

  @doc """
  Retrieves a user by UUID
  """
  def user_by_id(uuid) do
    with user <- Repo.get(User, uuid),
         %User{} <- user do
      {:ok, user}
    else
      nil -> {:error, :not_found}
      error -> {:error, error}
    end
  end

  def user_for_token(token) do
    with fetched_token <- Repo.get_by(Token, token: token),
         false <- is_nil(fetched_token),
         user <- Repo.get(User, fetched_token.user_id) do
      {:ok, user}
    else
      _ -> {:error, :not_found}
    end
  end

  def expire_token(%Token{} = token) do
    %{user_id: token.user_id, token: token.token}
    |> ExpireToken.new()
    |> Router.dispatch(consistency: :strong)
  end

  defp check_user_login({:error, :not_found}, _) do
    Auth.dummy_check_password()
    {:error, :invalid_username_or_password}
  end

  defp check_user_login({:ok, %User{} = user}, password) do
    case Auth.check_password(password, user.hashed_password) do
      true -> {:ok, user}
      false -> {:error, :invalid_username_or_password}
    end
  end

  defp generate_login_token({:ok, %User{} = user}) do
    generate_token =
      %{user_id: user.user_id}
      |> GenerateToken.new()
      |> GenerateToken.assign_token()
      |> GenerateToken.assign_expires()

    with :ok <- Router.dispatch(generate_token, consistency: :strong) do
      {:ok,
       %Token{user_id: user.user_id, token: generate_token.token, expires: generate_token.expires}}
    else
      reply -> reply
    end
  end

  defp generate_login_token({:error, err}) do
    {:error, err}
  end
end
