defmodule Rosemary.Auth do
  @moduledoc """
  Set of standard functions for handling authentication in Rosemary
  """

  alias Comeonin.Argon2
  alias NotQwerty123.PasswordStrength

  @doc """
  Hashes a string password
  """
  def hash_password(password) do
    Argon2.hashpwsalt(password)
  end

  @doc """
  Checks if a password is valid or not
  """
  def check_password(password, hash) do
    Argon2.checkpw(password, hash)
  end

  @doc """
  Fakes checking a password. Useful to prevent account enumeration.

  An attacker could guess which accounts are registered by guessing emails with made up passwords.
  By timing the round trip, accounts that are valid will take longer
  (due to the password hashing process) than non-existent accounts.
  By doing a dummy check, all accounts take about the same time,
  deterring this attack.
  """
  def dummy_check_password do
    Argon2.dummy_checkpw()
  end

  @doc """
  Checks a password's strength against NIST recommendations and the [HIBP](http://haveibeenpwned.com) database
  """
  def check_password_strength(password) do
    with :ok <- check_strong_password(password),
         :ok <- check_password_breach(password) do
      :ok
    else
      err -> err
    end
  end

  defp check_strong_password(password) do
    case PasswordStrength.strong_password?(password) do
      {:ok, _} -> :ok
      {:error, message} -> {:error, message}
    end
  end

  defp check_password_breach(password) do
    case ExPwned.Passwords.breached?(password) do
      false -> :ok
      true -> {:error, :password_breached}
      # Fail open if HIBP is down
      {:error, _} -> :ok
    end
  end
end
