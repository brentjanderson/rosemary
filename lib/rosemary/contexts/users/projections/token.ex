defmodule Rosemary.Users.Projections.Token do
  @moduledoc """
  Schema for user tokens
  """

  use Ecto.Schema

  @primary_key {:user_id, :binary_id, autogenerate: false}

  schema "users_tokens" do
    field :token, :string, unique: true, primary_key: true
    field :expires, :utc_datetime

    timestamps()
  end
end
