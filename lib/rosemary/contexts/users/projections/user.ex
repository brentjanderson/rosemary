defmodule Rosemary.Users.Projections.User do
  @moduledoc """
  Schema for user entities
  """

  use Ecto.Schema

  @primary_key {:user_id, :binary_id, autogenerate: false}

  schema "users" do
    field :email, :string, unique: true
    field :hashed_password, :string

    timestamps()
  end
end
