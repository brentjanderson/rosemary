defmodule Rosemary.Users.Queries.UserByEmail do
  @moduledoc """
  Query creator for getting a user by email
  """

  import Ecto.Query

  alias Rosemary.Users.Projections.User

  def new(email) do
    from u in User,
      where: u.email == ^email
  end
end
