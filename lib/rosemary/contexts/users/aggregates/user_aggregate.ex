defmodule Rosemary.Users.Aggregates.User do
  @moduledoc """
  User aggregate/write model to validate incoming commands and generate resulting events
  """

  defstruct [:user_id, :email, :hashed_password, :tokens]

  alias __MODULE__
  alias Rosemary.Users.Commands.{ExpireToken, GenerateToken, RegisterUser}
  alias Rosemary.Users.Events.{TokenExpired, TokenGenerated, UserRegistered}

  # public API

  @doc """
  Register a new user
  """
  def execute(%User{user_id: nil}, %RegisterUser{} = register) do
    %UserRegistered{
      user_id: register.user_id,
      email: register.email,
      hashed_password: register.hashed_password
    }
  end

  def execute(%User{user_id: user_id}, %GenerateToken{} = generate)
      when not is_nil(user_id) do
    %TokenGenerated{
      user_id: generate.user_id,
      token: generate.token,
      expires: generate.expires
    }
  end

  def execute(%User{user_id: user_id}, %ExpireToken{} = expire) when not is_nil(user_id) do
    %TokenExpired{
      user_id: expire.user_id,
      token: expire.token
    }
  end

  # state mutators

  def apply(%User{} = user, %UserRegistered{} = registered) do
    %User{
      user
      | user_id: registered.user_id,
        email: registered.email,
        hashed_password: registered.hashed_password,
        tokens: []
    }
  end

  def apply(%User{} = user, %TokenGenerated{} = generated) do
    %User{
      user
      | tokens: user.tokens ++ [generated.token]
    }
  end

  def apply(%User{} = user, %TokenExpired{} = expired) do
    %User{
      user
      | tokens: user.tokens -- [expired.token]
    }
  end
end
