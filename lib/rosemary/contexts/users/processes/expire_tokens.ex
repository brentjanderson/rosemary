defmodule Rosemary.Users.Processes.ExpireTokensPM do
  @moduledoc """
  Track user expiration tokens and expire them when they go invalid
  """

  use Commanded.ProcessManagers.ProcessManager,
    name: "Rosemary.Users.Processes.ExpireTokensPM",
    router: Rosemary.Router

  defstruct [
    :user_id,
    :tokens
  ]

  alias __MODULE__
  alias Rosemary.Users
  alias Rosemary.Users.Events.{TokenExpired, TokenGenerated, UserRegistered}

  def interested(%UserRegistered{user_id: user_id}), do: {:start, user_id}
  def interested(%TokenGenerated{user_id: user_id}), do: {:continue, user_id}
  def interested(%TokenExpired{user_id: user_id}), do: {:continue, user_id}
  # Add callback for %UserDeleted or %UserArchived when we get to that point
  def interested?(_event), do: false

  # State mutators

  @doc """
  Setup initial state when a user is registered
  """
  def apply(%ExpireTokensPM{} = expire, %UserRegistered{} = user) do
    %ExpireTokensPM{expire | user_id: user.user_id, tokens: user.tokens}
  end

  @doc """
  When a token is generated, set a timer to expire that token
  """
  def apply(%ExpireTokensPM{} = expire, %TokenGenerated{} = token) do
    with {:ok, parsed_expires, _} <- DateTime.from_iso8601(token.expires),
         now <- DateTime.utc_now(),
         delay <- DateTime.diff(parsed_expires, now, :milliseconds) do
      ref = Process.send_after(self(), {:token_expired, token}, delay)
      %ExpireTokensPM{expire | tokens: expire.tokens ++ [{token.token, ref}]}
    else
      err -> err
    end
  end

  @doc """
  When a token expires, remove it from the list
  """
  def apply(%ExpireTokensPM{} = expire, %TokenExpired{} = token) do
    new_tokens = expire.tokens |> Enum.filter(fn {t, _ref} -> t.token != token.token end)

    {_token, ref} =
      expire.tokens |> Enum.filter(fn {t, _ref} -> t.token == token.token end) |> List.first()

    Process.cancel_timer(ref)
    %ExpireTokensPM{expire | tokens: new_tokens}
  end

  @doc """
  Handle when a token expiration timeout occurs
  """
  def handle_info({:token_expired, token}, state) do
    Users.expire_token(token)
    {:noreply, state}
  end
end
