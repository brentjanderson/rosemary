defmodule Rosemary.Users.Projectors.User do
  @moduledoc """
  Project user events into read model
  """

  use Commanded.Projections.Ecto,
    name: "Users.Projectors.User",
    consistency: :strong

  alias Rosemary.Users.Events.{TokenGenerated, UserRegistered}
  alias Rosemary.Users.Projections.{Token, User}

  project(%UserRegistered{} = registered, fn multi ->
    Ecto.Multi.insert(multi, :users, %User{
      user_id: registered.user_id,
      email: registered.email,
      hashed_password: registered.hashed_password
    })
  end)

  project(%TokenGenerated{} = token, fn multi ->
    {:ok, expires, _} = DateTime.from_iso8601(token.expires)

    Ecto.Multi.insert(multi, :users_tokens, %Token{
      user_id: token.user_id,
      token: token.token,
      expires: expires
    })
  end)
end
