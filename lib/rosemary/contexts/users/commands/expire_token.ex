defmodule Rosemary.Users.Commands.ExpireToken do
  @moduledoc """
  Command to register a new user
  """

  defstruct [:user_id, :token]

  use ExConstructor
  use Vex.Struct

  alias Rosemary.Users.Validators.{ValidToken}

  validates(:user_id, uuid: true)

  validates(:token,
    presence: [message: "can't be empty"],
    string: true,
    by: &ValidToken.validate/2
  )

  validates(:expires,
    date: true
  )
end
