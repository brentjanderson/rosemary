defmodule Rosemary.Users.Commands.RegisterUser do
  @moduledoc """
  Command to register a new user
  """

  defstruct [:user_id, :email, :password, :hashed_password]

  alias __MODULE__
  alias Rosemary.Auth
  alias Rosemary.Users.Validators.{StrongPassword, ValidEmail}

  use ExConstructor
  use Vex.Struct

  validates(:user_id, uuid: true)

  validates(:email,
    presence: [message: "can't be empty"],
    format: [with: ~r/\S+@\S+\.\S+/, allow_nil: true, allow_blank: true, message: "is invalid"],
    string: true,
    by: &ValidEmail.validate/2
  )

  validates(:hashed_password,
    presence: [message: "can't be empty"],
    string: true
  )

  validates(:password,
    presence: [message: "can't be empty"],
    string: true,
    by: &StrongPassword.validate/2
  )

  def assign_id(%RegisterUser{} = register_user, uuid) do
    %RegisterUser{register_user | user_id: uuid}
  end

  def downcase_email(%RegisterUser{email: email} = register_user) do
    %RegisterUser{register_user | email: String.downcase(email)}
  end

  def hash_password(%RegisterUser{password: password} = register_user) do
    %RegisterUser{register_user | hashed_password: Auth.hash_password(password)}
  end
end
