defmodule Rosemary.Users.Commands.GenerateToken do
  @moduledoc """
  Command to register a new user
  """

  defstruct [:user_id, :token, :expires]

  alias __MODULE__

  use ExConstructor
  use Vex.Struct

  @token_length 128
  # 1 day out
  @expires_in_secs 24 * 60 * 60

  validates(:user_id, uuid: true)

  validates(:token, presence: [message: "can't be empty"], string: true)

  validates(:expires,
    datetime: true
  )

  @doc """
  Generate and attach a token
  """
  def assign_token(%GenerateToken{} = cmd) do
    token = @token_length |> :crypto.strong_rand_bytes() |> :base64.encode()
    %GenerateToken{cmd | token: token}
  end

  @doc """
  Attach expiration time
  """
  def assign_expires(%GenerateToken{} = cmd) do
    expires =
      Timex.now()
      |> Timex.add(Timex.Duration.from_seconds(@expires_in_secs))
      |> DateTime.truncate(:second)

    %GenerateToken{cmd | expires: expires}
  end
end
