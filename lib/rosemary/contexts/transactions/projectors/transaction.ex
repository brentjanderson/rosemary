defmodule Rosemary.Transactions.Projectors.Transaction do
  @moduledoc """
  Project transaction events into read model
  """

  use Commanded.Projections.Ecto,
    name: "Transactions.Projectors.Transaction",
    consistency: :strong

  alias Rosemary.Transactions.Events.{TransactionAdded}
  alias Rosemary.Transactions.Projections.Transaction

  project(%TransactionAdded{} = added, fn multi ->
    {:ok, parsed_date} = Date.from_iso8601(added.date)

    Ecto.Multi.insert(multi, :transactions, %Transaction{
      txn_id: added.txn_id,
      user_id: added.user_id,
      category: added.category,
      merchant: added.merchant,
      amount: added.amount,
      date: parsed_date
    })
  end)
end
