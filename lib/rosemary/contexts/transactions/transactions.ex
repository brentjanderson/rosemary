defmodule Rosemary.Transactions do
  @moduledoc """
      Transaction context to support Transaction public api
  """

  alias Rosemary.Repo
  alias Rosemary.Router
  alias Rosemary.Transactions.Commands.AddTransaction
  alias Rosemary.Transactions.Projections.Transaction
  alias Rosemary.Transactions.Queries.TxnsByUserID

  def get_transaction!(uuid) do
    case Repo.get(Transaction, uuid) do
      nil -> {:error, :not_found}
      projection -> {:ok, projection}
    end
  end

  def get_user_transactions(user_id) do
    txns =
      user_id
      |> TxnsByUserID.new()
      |> Repo.all()

    {:ok, txns}
  end

  def add_transaction(user_id, category, merchant, amount, date \\ Date.utc_today()) do
    uuid = UUID.uuid4()

    new_transaction =
      %{user_id: user_id, category: category, merchant: merchant, amount: amount, date: date}
      |> AddTransaction.new()
      |> AddTransaction.assign_id(uuid)

    with :ok <- Router.dispatch(new_transaction, consistency: :strong) do
      get_transaction!(uuid)
    else
      reply -> reply
    end
  end
end
