defmodule Rosemary.Transactions.Queries.TxnsByUserID do
  @moduledoc """
  Query creator for getting transactions by user IDs
  """

  import Ecto.Query

  alias Rosemary.Transactions.Projections.Transaction

  def new(uuid) do
    from t in Transaction,
      where: t.user_id == ^uuid
  end
end
