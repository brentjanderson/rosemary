defmodule Rosemary.Transactions.Aggregates.Transaction do
  @moduledoc """
  Transaction aggregate/write model to validate incoming commands and generate resulting events
  """

  defstruct [:txn_id, :user_id, :category, :merchant, :amount, :date]

  alias __MODULE__
  alias Rosemary.Transactions.Commands.{AddTransaction}
  alias Rosemary.Transactions.Events.{TransactionAdded}

  # public API

  @doc """
  Add a new transaction
  """
  def execute(%Transaction{txn_id: nil}, %AddTransaction{} = add_txn) do
    %TransactionAdded{
      txn_id: add_txn.txn_id,
      user_id: add_txn.user_id,
      category: add_txn.category,
      merchant: add_txn.merchant,
      amount: add_txn.amount,
      date: add_txn.date
    }
  end

  # state mutators

  def apply(%Transaction{} = txn, %TransactionAdded{} = txn_added) do
    %Transaction{
      txn
      | txn_id: txn_added.txn_id,
        user_id: txn_added.user_id,
        category: txn_added.category,
        merchant: txn_added.merchant,
        amount: txn_added.amount,
        date: txn_added.date
    }
  end
end
