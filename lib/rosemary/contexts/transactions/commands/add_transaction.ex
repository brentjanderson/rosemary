defmodule Rosemary.Transactions.Commands.AddTransaction do
  @moduledoc """
  Command to create a new transaction
  """

  defstruct [:txn_id, :user_id, :category, :merchant, :amount, :date]

  alias __MODULE__

  use ExConstructor
  use Vex.Struct

  validates(:txn_id, uuid: true)
  validates(:user_id, uuid: true)

  validates(:category,
    presence: [message: "can't be empty"],
    string: true
  )

  validates(:merchant,
    presence: [message: "can't be empty"],
    string: true
  )

  validates(:amount,
    presence: [message: "can't be empty"],
    number: true
  )

  validates(:date,
    date: true
  )

  def assign_id(%AddTransaction{} = add_transaction, uuid) do
    %AddTransaction{add_transaction | txn_id: uuid}
  end
end
