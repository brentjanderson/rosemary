defmodule Rosemary.Transactions.Events.TransactionAdded do
  @moduledoc """
  Event to reflect a new transaction
  """

  defstruct [:txn_id, :user_id, :category, :merchant, :amount, :date]
end
