defmodule Rosemary.Transactions.Projections.Transaction do
  @moduledoc """
  Schema for transaction entities
  """

  use Ecto.Schema

  @primary_key {:txn_id, :binary_id, autogenerate: false}

  schema "transactions" do
    field :user_id, :binary_id
    field :category, :string
    field :merchant, :string
    field :amount, :integer
    field :date, :date

    timestamps()
  end
end
