defmodule Rosemary.Transactions.Supervisor do
  @moduledoc """
  Supervision tree for Transactions context, including projections
  """

  use Supervisor

  alias Rosemary.Transactions.Projectors.Transaction

  def start_link do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_arg) do
    Supervisor.init(
      [
        Transaction
      ],
      strategy: :one_for_one
    )
  end
end
