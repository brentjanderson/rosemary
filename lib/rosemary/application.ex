defmodule Rosemary.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  alias RosemaryWeb.Endpoint

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      # Start the Ecto repository
      supervisor(Rosemary.Repo, []),

      # Start the endpoint when the application starts
      supervisor(RosemaryWeb.Endpoint, []),

      # Users supervisor
      supervisor(Rosemary.Users.Supervisor, []),

      # Transactions supervisor
      supervisor(Rosemary.Transactions.Supervisor, [])
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Rosemary.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    Endpoint.config_change(changed, removed)
    :ok
  end
end
