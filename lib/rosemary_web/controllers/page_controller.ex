defmodule RosemaryWeb.PageController do
  use RosemaryWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
