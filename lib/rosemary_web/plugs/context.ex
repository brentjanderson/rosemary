defmodule RosemaryWeb.Context do
  @moduledoc """
  Plug to extract authorization header, match it to a session, and authenticate
  """

  @behaviour Plug

  import Plug.Conn

  alias Rosemary.Users

  def init(opts), do: opts

  def call(conn, _) do
    context = build_context(conn)
    Absinthe.Plug.put_options(conn, context: context)
  end

  @doc """
  Return the current user context based on the authorization header
  """
  def build_context(conn) do
    with ["Bearer " <> token] <- get_req_header(conn, "authorization"),
         {:ok, user_id} <- authorize(token) do
      %{user_id: user_id}
    else
      _ -> %{}
    end
  end

  defp authorize(token) do
    case Users.user_for_token(token) do
      {:ok, %{user_id: user_id}} -> {:ok, user_id}
      err -> err
    end
  end
end
