defmodule RosemaryWeb.Router do
  use RosemaryWeb, :router

  use Plug.ErrorHandler
  use Sentry.Plug

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :graphql do
    plug RosemaryWeb.Context
  end

  scope "/", RosemaryWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api" do
    pipe_through :graphql

    forward "/graphql", Absinthe.Plug, schema: Rosemary.Schema

    forward "/graphiql",
            Absinthe.Plug.GraphiQL,
            schema: Rosemary.Schema
  end
end
