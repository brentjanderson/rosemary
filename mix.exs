defmodule Rosemary.MixProject do
  @moduledoc """
  Mix file for Rosemary app
  """

  use Mix.Project

  def project do
    [
      app: :rosemary,
      version: "0.1.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: Coverex.Task],
      dialyzer: [ignore_warnings: "dialyzer.ignore-warnings"]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Rosemary.Application, []},
      extra_applications: [:sentry, :logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.4.0"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.0"},
      {:ecto_sql, "~> 3.0"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 2.11"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      {:sentry, "~> 7.0"},
      {:absinthe, "~> 1.4"},
      {:absinthe_plug, "~> 1.4"},
      {:commanded, "~> 0.17"},
      {:commanded_extreme_adapter, "~> 0.4"},
      {:commanded_ecto_projections,
       github: "commanded/commanded-ecto-projections",
       ref: "edcc5284acd81574c7fe9cab2bb093b90239d679"},
      {:elixir_uuid, "~> 1.1"},
      {:comeonin, "~> 4.1"},
      {:not_qwerty123, "~> 2.3"},
      {:ex_pwned, "~> 0.1.0"},
      {:argon2_elixir, "~> 1.2"},
      {:email_checker, "~> 0.1.2"},
      {:burnex, "~> 1.1"},
      {:exconstructor, "~> 1.1"},
      {:vex, "~> 0.8.0"},
      {:timex, "~> 3.4"},
      {:distillery, "~> 2.0", runtime: false},
      {:dialyxir, "~> 1.0.0-rc.4", only: [:dev], runtime: false},
      {:credo, "~> 1.0", only: [:dev, :test], runtime: false},
      {:faker, "~> 0.11.2", only: [:test]},
      {:mix_test_watch, "~> 0.8", only: :dev, runtime: false},
      {:coverex, "~> 1.5", only: :test},
      {:phoenix_live_reload, "~> 1.2", only: :dev}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ectod.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
