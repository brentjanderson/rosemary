defmodule Rosemary.TransactionsTest do
  @moduledoc """
  Tests for the Transactions context
  """

  use Rosemary.DataCase

  alias Rosemary.{Transactions, Users}
  alias Rosemary.Transactions.Projections.Transaction
  alias Rosemary.Users.Projections.User

  describe "get_user_transactions" do
    test "should return transactions for a given user" do
      email = Faker.Internet.free_email()
      password = Faker.String.base64(16)
      assert {:ok, %User{} = user} = Users.register_user(email, password)

      assert {:ok, %Transaction{} = txn} =
               Transactions.add_transaction(user.user_id, "Fake category", "Fake merchant", 1234)

      assert {:ok, [%Transaction{} = fetched_txn | rest]} =
               Transactions.get_user_transactions(user.user_id)
    end
  end

  describe "add_transaction" do
    test "creating a transaction" do
      email = Faker.Internet.free_email()
      password = Faker.String.base64(16)
      assert {:ok, %User{} = user} = Users.register_user(email, password)

      assert {:ok, %Transaction{} = txn} =
               Transactions.add_transaction(user.user_id, "Fake category", "Fake merchant", 1234)

      assert txn.user_id == user.user_id
    end
  end
end
