defmodule Rosemary.UsersTest do
  @moduledoc """
  Tests for the Users context
  """

  use Rosemary.DataCase

  alias Rosemary.Users
  alias Rosemary.Users.Projections.{Token, User}

  describe "register user" do
    test "should succeed with valid data" do
      email = Faker.Internet.free_email()
      password = Faker.String.base64(16)
      assert {:ok, %User{} = user} = Users.register_user(email, password)
      assert user.email == email
      assert Comeonin.Argon2.checkpw(password, user.hashed_password)
    end

    test "should fail when duplicating email" do
      email = Faker.Internet.free_email()
      password = Faker.String.base64(16)
      assert {:ok, %User{} = user} = Users.register_user(email, password)

      assert {:error, :validation_failure, %{email: [:email_already_exists]}} =
               Users.register_user(email, password)
    end

    test "should fail with weak passwords" do
      email = Faker.Internet.free_email()
      password = Faker.String.base64(1)

      assert {:error, :validation_failure,
              %{password: ["The password should be at least 8 characters long."]}} =
               Users.register_user(email, password)
    end
  end

  describe "login_user" do
    test "should login with valid user" do
      email = Faker.Internet.free_email()
      password = Faker.String.base64(16)
      assert {:ok, %User{} = user} = Users.register_user(email, password)
      assert {:ok, %Token{} = token} = Users.login_user(email, password)
      assert token.user_id == user.user_id
    end

    test "should reject fake users" do
      email = Faker.Internet.free_email()
      password = Faker.String.base64(16)
      assert {:error, :invalid_username_or_password} = Users.login_user(email, password)
    end

    test "should reject fake passwords" do
      email = Faker.Internet.free_email()
      password = Faker.String.base64(16)
      assert {:ok, %User{}} = Users.register_user(email, password)
      assert {:error, :invalid_username_or_password} = Users.login_user(email, "something fake")
    end
  end

  describe "user_for_token" do
    test "should return a valid token for valid login" do
      email = Faker.Internet.free_email()
      password = Faker.String.base64(16)
      assert {:ok, %User{} = user} = Users.register_user(email, password)
      assert {:ok, %Token{} = token} = Users.login_user(email, password)
      Process.sleep(1000)
      assert {:ok, %User{} = fetched_user} = Users.user_for_token(token.token)
      assert fetched_user.user_id == user.user_id
    end
  end

  describe "user_by_email" do
    test "should retrieve users that exist" do
      email = Faker.Internet.free_email()
      password = Faker.String.base64(16)
      assert {:ok, %User{}} = Users.register_user(email, password)
      assert {:ok, %User{}} = Users.user_by_email(email)
    end

    test "should return error for nonexistent users" do
      email = Faker.Internet.free_email()
      assert {:error, :not_found} == Users.user_by_email(email)
    end
  end

  describe "user_by_id" do
    test "should retrieve users that exist" do
      email = Faker.Internet.free_email()
      password = Faker.String.base64(16)
      assert {:ok, %User{} = user} = Users.register_user(email, password)
      {:ok, fetched_user} = Users.user_by_id(user.user_id)
      assert fetched_user.user_id == user.user_id
    end

    test "should return :error for nonexistent users" do
      uuid = UUID.uuid4()
      assert {:error, :not_found} == Users.user_by_id(uuid)
    end
  end
end
