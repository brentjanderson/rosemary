defmodule RosemaryWeb.PageControllerTest do
  @moduledoc """
  Test for Phoenix Frontend

  Currently more of a sanity check that Phoenix can render the main route
  """

  use RosemaryWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Sorry - this app requires Javascript."
  end
end
