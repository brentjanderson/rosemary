# Requirements

- [ ] Enter & Categorize Transactions
- [ ] CSV file upload for bulk import
- [ ] Graph or two for expense reporting
- [ ] Filtering & sorting table
- [ ] Convert dollar amounts to Roman Numerals
