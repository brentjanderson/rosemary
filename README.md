# Rosemary

> Simple, well-seasoned personal budgeting app. Built in Elixir and React, with love.

## Getting Started

0. Ensure you've got Docker installed
1. `db/start.sh`
1. `db/start_eventstore.sh`
1. Wait a moment for the containers to get moving
1. `mix do ecto.create, ecto.migrate`
1. `mix phx.server`

This should start the application at https://localhost:4000

# Notes

This application is a labor of love incorporating some of my favorite things about Elixir.
The application is structured around two core bounded contexts: Transactions and Users.
Although in a production application these could have started out under one context, I wanted to have two to make the application feel more like a typical production application. Each bounded context is fully tested, and can be tested with the following command:

```
MIX_ENV=test mix do ecto.drop, ecto.create,ecto.migrate && rm -rf db/eventstore && docker kill eventstore-node&& sh db/start_eventstore.sh && mix test
```

I also used dialyzer to analyze the application, and it seems to choke a bit on `commanded` and `vex`,
but it was very useful for building the application.

I developed the application in a TDD-style, with tests focusing primarily on the core contexts. Additional
tests would be beneficial for the GraphQL resolvers and a few other pieces of the application.

## Event Sourcing

Both the `Users` context and the `Transactions` context are persisted with event sourcing, supported by the wonderful [commanded](https://github.com/commanded/commanded) framework. Event sourcing is a natural fit for a budgeting app, since it is built around commands, events, and transactions. Although event sourcing deserves a more thorough writeup than I can afford to fill in here, the gist is that:

- Each GraphQL query or mutation only talks to the context associated with the action being taken
- Mutating actions on a context (e.g. add_transaction) issue a command to the Event Sourcing system
- This command is constructed as a struct, validated with Vex (to keep validation logic local to each command), and (if valid) passed to the associated context aggregate
- The aggregate handles generating events corresponding to each command (e.g. TransactionAdded)
- Events are applied to the aggregate, and are propagated elsewhere, including projectors which update the database with any needed changes
- Queries simply read the database

The above approach has the following benefits:

- Each slice of functionality is well defined and encapsulated
- Event sourcing is not only a good fit for expense tracking, but also a good fit for Elixir - processes, GenServers, and the functional nature of a ledger make for a good combination
- ES also pairs well with GraphQL since it offers a strict API for mutations and queries
- With further time, realtime subscriptions would be trivial to implement for updating the UI over Absinthe subscriptions with an event handler process
- System evolution gets some great bonuses for evolving read models or running reports, since no data is lost over time

The drawbacks:

- For the unfamiliar, the code layout is a bit foreign
- The users context is a bit contrived, as Event Sourcing doesn't necessarily pair well with authentication and authorization

## GraphQL

This piece is a pretty straightforward coupling between the GraphQL schema and the context APIs for Users and Transactions. A standard Plug for capturing the authentication token and assigning the user_id to the GraphQL context was the most complex piece, and that was boilerplate copied verbatim from the Absinthe docs with a few minor adjustments.

## Frontend

The frontend is a pretty straightforward milligram + react + apollo client setup. This is the smallest part of the overall puzzle, and was (happily) pretty easy to get put together. The fractal composition of react components/functions + GraphQL makes frontend in this framework a joy. I had hoped that, if the backend had been faster, I would be able to also include a React Native app as well. Even so, I'm mildly pleased with the frontend. It could use a lot of polish, as noted below.

## Missing pieces

- The user system is very bare and lacks password resets, email confirmations, etc. If I had more time, I'd wire up Swoosh and be off to the races.
- No transaction editing is in place in this version, but it would be trivial to implement with more time.
- I stubbed out a process manager for expiring user tokens, but did not activate it in the supervision tree to save time. It is not active code.
- The frontend needs some work - authentication tokens are not reset when you refresh the browser, which is not convenient
- The frontend could do with more error coverage as well. I focused primarily on the backend for this exercise, but could extend this with more error UI for a production application
- I wanted to deploy this with Docker and Docker Compose, however some of the internal dependencies have been rough (uuid vs elixir_uuid - some dependencies in Commanded haven't updated yet). This made building it more challenging than I'd like, although I run a side project that is a Dockerized Phoenix application and have for over a year.

## Conclusion

Although the Thanksgiving holiday and accompanying travel for a week made it difficult to complete this application in the timeline I intended, I have still put in a ton of time (more than I initially planned) to get this to where it is at. It was a worthwhile exercise for my own enjoyment, and I hope it demonstrates my interests and competencies with Elixir and software engineering in general.
